import os
import platform

if str(platform.system()) == 'Linux':
    print('Setting up inventory virtual host...')
    os.system('gnome-terminal -e \'/bin/bash -c \'sudo rabbitmq-server start; sudo rabbitmqctl add_vhost /inventory; sudo rabbitmqctl set_permissions -p /inventory guest ".*" ".*" ".*"\'\'')

    print('Setting up user virtual host...')
    os.system('gnome-terminal -e \'/bin/bash -c \'sudo rabbitmq-server start; sudo rabbitmqctl add_vhost /users; sudo rabbitmqctl set_permissions -p /users guest ".*" ".*" ".*"\'\'')

    print('Starting Inventory Worker...')
    os.system('gnome-terminal -e \'/bin/bash -c "cd inventory_submodule; celery -A inventory_tasks.cel worker -n inventory --loglevel=ERROR"\'')

    print('Starting Inventory Worker...')
    os.system('gnome-terminal -e \'/bin/bash -c "cd user_submodule; celery -A user_tasks.cel worker -n users --loglevel=ERROR"\'')

    print('Starting Submodules...')
    os.system('gnome-terminal -e \'/bin/bash -c "cd inventory_submodule; python3 main.py"\'')
    os.system('gnome-terminal -e \'/bin/bash -c "cd user_submodule; python3 main.py"\'')

elif str(platform.system()) == 'Windows':
    print('Setting up inventory virtual host...')
    os.system('start cmd /c "rabbitmq-server start & rabbitmqctl add_vhost /inventory & rabbitmqctl set_permissions -p /inventory guest ".*" ".*" ".*""')

    print('Setting up user virtual host...')
    os.system('start cmd /c "rabbitmq-server start & rabbitmqctl add_vhost /users & rabbitmqctl set_permissions -p /users guest ".*" ".*" ".*""')

    print('Starting Inventory Worker...')
    os.system('start cmd /c "cd inventory_submodule & celery -A inventory_tasks.cel -P eventlet worker -n inventory --loglevel=ERROR"')

    print('Starting User Worker...')
    os.system('start cmd /c "cd user_submodule & celery -A user_tasks.cel -P eventlet worker -n users --loglevel=ERROR"')

    print('Starting Submodules...')
    os.system('start cmd /c "cd user_submodule & python main.py"')
    os.system('start cmd /c "cd inventory_submodule & python main.py"')

elif str(platform.system()) == 'Darwin':
    # dear brittania
    # i cant test this, it should open 4 new terminals, 2 celery servers and 2 flask apps
    # if there is an issue it is probably with the chaining of commands(the & parts) or not having permissions
    # so add sudo or whatever macs version is before hand, the file can't just be ran with sudo since
    # it is writing and executing scripts
    # another potential issue is the cd command may not be part of the builtin commands, similiar to Linux
    # so youll need to figure out where cds command is being stored on your computer and link it into the script somehow
    # but all the scripting examples i found had cd in plain text so it shouldnt be an issue
    # i also dont know if background terminals terminate on completion of the script or not
    import applescript

    print('Setting up inventory virtual host...')
    applescript.tell.app("Terminal", 'do script "rabbitmq-server start" & do script "rabbitmqctl add_vhost /inventory" & do script "rabbitmqctl set_permissions -p /inventory guest ".*" ".*" ".*""', background=True)

    print('Setting up inventory virtual host...')
    applescript.tell.app("Terminal", 'do script "rabbitmq-server start" & do script "rabbitmqctl add_vhost /users" & do script "rabbitmqctl set_permissions -p /users guest ".*" ".*" ".*""', background=True)

    print('Starting Inventory Worker...')
    applescript.tell.app("Terminal", 'do script "cd inventory_submodule" & do script "celery -A inventory_tasks.cel -n inventory --loglevel=ERROR"', background=True)

    print('Starting User Worker...')
    applescript.tell.app("Terminal", 'do script "cd user_submodule" & do script "celery -A user_tasks.cel -n users --loglevel=ERROR"', background=True)

    print('Starting Submodules...')
    applescript.tell.app("Terminal", 'do script "cd user_submodule" & do script "python3 main.py"', background=True)
    applescript.tell.app("Terminal", 'do script "cd inventory_submodule" & do script "python3 main.py"', background=True)
