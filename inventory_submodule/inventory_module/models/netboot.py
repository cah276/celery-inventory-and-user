# inventory/models/netboot.py
from inventory_api.extensions import BaseModel
from sqlalchemy import Column, Integer, String, Boolean
from .options import BootOptionsModel
from sqlalchemy.orm import relationship


class NetbootModel(BaseModel):
    __tablename__ = 'netboot'
    __repr_attr__ = ['hostname']

    id = Column(Integer, primary_key=True)
    hostname = Column('tag', String(80), unique=True)
    location = Column('location', String(80))
    mac_address = Column('mac_address', String(80))
    approved = Column('bootable', Boolean(), default=False)
    active = Column('active', Boolean(), default=True)

    boot_options = relationship(
        BootOptionsModel,
        secondary=inventory_boot_opt_association,
        backref="inventory",
        lazy="dynamic"
    )
