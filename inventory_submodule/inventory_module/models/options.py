# inventory/models/options.py
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey
from sqlalchemy.orm import relationship, backref
from extensions import BaseModel


class BootOptionsModel(BaseModel):
    __tablename__ = 'inventory_boot_options'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(80), unique=True, nullable=False)
    active = Column('active', Boolean(), default=True)

    @classmethod
    def find_by_boot_option(cls, name):
        return cls.query.filter_by(name=name).first()

    def __str__(self):
        return self.name


class InventoryFormOptionsType(BaseModel):
    __tablename__ = 'inventory_form_options_type'

    id = Column('id', Integer, primary_key=True)
    options_type = Column('options_type', String(80), nullable=False)
    models = relationship("InventoryFormOptionsModel", backref="options type")
    has_mac_address = Column(Boolean)
    has_boot_options = Column(Boolean)
    active = Column(Boolean, default=True)

    @classmethod
    def find_by_options_type(cls, options_type):
        return cls.query.filter_by(options_type=options_type).first()

    def __str__(self):
        return self.options_type


class InventoryFormOptionsModel(BaseModel):
    __tablename__ = 'inventory_form_options_model'

    id = Column('id', Integer, primary_key=True)
    options_model = Column('options_model', String(80),
                           unique=True, nullable=False)
    option_id = Column(Integer, ForeignKey('inventory_form_options_type.id'))
    active = Column(Boolean, default=True)

    @classmethod
    def find_by_options_model(cls, options_model):
        return cls.query.filter_by(options_model=options_model).first()

    @classmethod
    def search_options_model(cls, params):
        param = '%' + params + '%'
        return list(map(lambda x: x.json(), cls.query.filter(cls.options_model.like(param)).all()))

    def __str__(self):
        return self.options_model

    def __repr__(self):
        return self.options_model
