# # inventory/serializers.py
from marshmallow_sqlalchemy import ModelSchema

from marshmallow import fields

from .inventory import InventoryModel
from .options import BootOptionsModel, InventoryFormOptionsType, InventoryFormOptionsModel


class BootOptionsSchema(ModelSchema):
    class Meta:
        model = BootOptionsModel


class InventorySchema(ModelSchema):
    class Meta:
        model = InventoryModel


class InventoryFormOptionsSchema(ModelSchema):
    class Meta:
        model = InventoryFormOptionsType


class InventoryFormOptionsModelSchema(ModelSchema):
    class Meta:
        model = InventoryFormOptionsModel
