# inventory/__init__.py
import os
from flask import Blueprint, Flask
from flask_restful import Api
from extensions import (
    api, cors, mail, route_debugger,
    ma, init_db, get_session, BaseModel
)

from celery import Celery

celery = Celery('inventory_util', broker='amqp://localhost//inventory', backend='amqp://')
cel_user = Celery('inventory_user', broker='amqp://localhost//users', backend='amqp://')

""" Startup Configuration """
app = Flask(__name__)

"""
Initialize module configuration
Loads the settings for extensions
"""
import config

inventory_bp = Blueprint('inventory_bp', __name__)
inventory_api = Api(inventory_bp)

app.register_blueprint(inventory_bp)

""" Register routes for modules """
import inventory_module.routes

""" Make a folder for image resources """
os.makedirs(app.config['INVENTORY_IMAGES_DIR'], exist_ok=True)

engine, session = get_session(app)
BaseModel.set_session(session)

"""
Configure security for the application
"""

import inventory_module

init_db(app, engine, session)

api.init_app(app)                               # Manage application routes
cors.init_app(app, supports_credentials=True)   # Handle cross realm traffic
ma.init_app(app)                                # Serialization for app models
mail.init_app(app)                              # Manage mailing server

#route_debugger.init_app(app)                    # Browser based debug information
