import os


INVENTORY_IMAGES_DIR = os.path.abspath("instance/images/inventory")

INVENTORY_MODULE_ROUTES = {
    "Inventory": {
        "url": "/api/inventory",
        "methods": {
            "PUT": "update one",
            "POST": "create one"
        }
    },

    "GetOneInventory": {
        "url": "/api/inventory/one",
        "methods": {
            "POST": "get one"
        }
    },
    "InventoryDeactivate": {
        "url": "/api/inventory/deactivate",
        "methods": {
            "PUT": "deactivate",
        }
    },
    "InventoryList": {
        "url": "/api/inventory/all",
        "methods": {
            "GET": "read all"
        }
    },
    "BootOptionsList": {
        "url": "/api/inventory/boot_options_list",
        "methods": {
            "GET": "get item boot options"
        }
    },
    "BootOptions": {
        "url": "/api/inventory/boot_options",
        "methods": {
            "POST": "add a new boot option",
            "PUT": "deactivate a boot option"
        }
    },
    "InventoryFormType": {
        "url": "/api/inventory/form_options/type",
        "methods": {
            "GET": "read form type",
            "POST": "create form type",
            "PUT": "deactivate form type"
        }
    },
    "InventoryFormModel": {
        "url": "/api/inventory/form_options/model",
        "methods": {
            "GET": "get model options",
            "POST": "create model options",
            "PUT": "deactivate form model"
        }
    },
    "InventoryFormModelSearch": {
        "url": "/api/inventory/form_options/model/search",
        "methods": {
            "GET": "search model options"
        }
    },
    "InventoryListWithPagination": {
        "url": "/api/inventory/pagination/all",
        "methods": {
            "POST": "pagination all"
        }
    },

    "InventoryActiveWithPagination": {
        "url": "/api/inventory/pagination/active",
        "methods": {
            "POST": "pagination active"
        }
    },
    "InventoryInactiveWithPagination": {
        "url": "/api/inventory/pagination/inactive",
        "methods": {
            "POST": "pagination inactive"
        }
    },
    "InventorySearchList": {
        "url": "/api/inventory/search",
        "methods": {
            "GET": "search"
        }
    },
    "QueryInventoryByTag": {
        "url": "/api/inventory/search_by_tag",
        "methods": {
            "POST": "search by tag"
        }
    },
    "InventoryAdvanceSearch": {
        "url": "/api/inventory/advanced",
        "methods": {
            "GET": "advanced search"
        }
    },
    "InventoryItemCount": {
        "url": "/api/inventory/count",
        "methods": {
            "GET": "get item count"
        }
    },
    "InventoryImage": {
        "url": "/api/inventory/image",
        "methods": {
            "GET": "get image"
        }
    }
}

# DEFAULT_ITEMS = {
#     "000061": {
#         "serial": "LOPDA",
#         "type": "keyboard",
#         "model": "Dell",
#         "description": "dell membrane keyboard",
#         "buydate": "2018-05-01 14:58:19",
#         "warranty": "2018-05-01 14:58:19",
#         "location": "DERR 590",
#         "prev_location": "DERR 231",
#         "active": True,
#         "mac_address": "",
#         "file_name": ""
#     },
#     "128238": {
#         "serial": "FP7RF56",
#         "type": "tablet",
#         "model": "Fire 7",
#         "description": "this is an amazon product",
#         "buydate": "2018-05-01 14:58:19",
#         "warranty": "2018-05-01 14:58:19",
#         "location": "AVRY 301",
#         "prev_location": "NA",
#         "active": True,
#         "mac_address": "",
#         "file_name": ""
#     },
#     "311663": {
#         "serial": "9JFGFN1",
#         "type": "tablet",
#         "model": "iPad",
#         "description": "iPad 2",
#         "buydate": "2018-05-01 14:58:19",
#         "warranty": "2018-05-01 14:58:19",
#         "location": "DERR 325",
#         "prev_location": "NA",
#         "active": False,
#         "mac_address": "",
#         "file_name": ""
#     },
#     "777555": {
#         "serial": "KLPO09",
#         "type": "tablet",
#         "model": "iPad",
#         "description": "iPad Pro",
#         "buydate": "2018-04-12 16:41:17",
#         "warranty": "2018-05-01 14:58:19",
#         "location": "DERR 325",
#         "prev_location": "COM 231",
#         "active": True,
#         "mac_address": "",
#         "file_name": ""
#     },
#     "787921": {
#         "serial": "KLPD00",
#         "type": "tablet",
#         "model": "iPad Pro",
#         "description": "IPad",
#         "buydate": "2018-05-04 11:51:58",
#         "warranty": "2018-05-01 14:58:19",
#         "location": "COMAL 222",
#         "prev_location": "COMAL 231",
#         "active": False,
#         "mac_address": "",
#         "file_name": ""
#     }
# }

DEFAULT_BOOT_OPTIONS = {
    "1": "Option 1",
    "2": "Options 2",
    "3": "Options 3"
}

DEFAULT_FORM_TYPES = {
    "1": {"type": "tablet", "boot_options": False, "mac_address": False},
    "2": {"type": "computer", "boot_options": True, "mac_address": True},
    "3": {"type": "keyboard", "boot_options": False, "mac_address": False}
}


INVENTORY_ROLES = ("_inventory | admin", "_inventory | user", "_inventory | agent")

VALID_EXTENSIONS = [".jpeg", ".jpg", ".png", ".tiff"]
