# inventory/utilities.py
from datetime import datetime
from dateutil.parser import parse as date_parser


def get_dt(datetime_obj):

    datetime_obj.split(".")

    date = datetime_obj.split(".")

    date = datetime.strptime(
        str(date_parser(date[0], ignoretz=True, )),
        '%Y-%m-%d %H:%M:%S'
    )

    return date
