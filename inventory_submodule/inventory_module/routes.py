# inventory/routes.py
from importlib import import_module

from inventory_module import app
from . import inventory_api


with app.app_context():
    api_config = app.config.get("INVENTORY_MODULE_ROUTES")
    if api_config:
        inventory_resources = import_module("inventory_module.controllers")
        for api_name in api_config.keys():
            if api_config[api_name].get("url") is None:
                continue
            api_resource = getattr(inventory_resources, api_name)
            inventory_api.add_resource(
                api_resource, *(api_config[api_name]['url'], api_config[api_name]['url'] + '/')
            )
    else:
        print("Error: Failed to load routes for the inventory module.")
