from celery import Celery
from inventory_module.models import InventoryModel
from sqlalchemy.exc import DatabaseError
from dateutil.parser import parse as date_parser
from datetime import datetime
import uuid
import os
from flask import current_app
from werkzeug.utils import secure_filename


def valid_extension(file_name):
    if not file_name.endswith(tuple(current_app.config['VALID_EXTENSIONS'])):
        raise AttributeError("Invalid extension.")
    return file_name

def get_filename(file):
    try:
        name, ext = os.path.splitext(file.filename)
    except AttributeError:
        raise AttributeError('No filename for image provided.')
    else:
        return str(uuid.uuid4()) + ext

cel = Celery('inventory', broker='amqp://localhost//inventory', backend='amqp://')
cel_user = Celery('inventory_user', broker='amqp://localhost//users', backend='amqp://')

@cel.task(name="inventory.add_item")
def add_item(item, file):
    try:
        file_name = None
        if file:
            file_name = secure_filename(
                valid_extension(get_filename(file))
            )
            file.save(
                os.path.join(current_app.config['INVENTORY_IMAGES_DIR'], file_name)
            )

        inventory = InventoryModel.create(
            tag=item['tag'],
            serial=item['serial'],
            type=item['type'],
            description=item['description'],
            model=item['model'],
            buydate=datetime.strptime(
                str(date_parser(item['buydate'], ignoretz=True, )),
                '%Y-%m-%d %H:%M:%S'
            ),
            warranty=datetime.strptime(
                str(date_parser(item['warranty'], ignoretz=True, )),
                '%Y-%m-%d %H:%M:%S'
            ),
            location=item['location'],
            prev_location=item['prev_location'],
            active=item['active'],
            file_name=file_name,
            owner_id=item['owner_id']
        )

        cel_user.send_task("users.give_ownership", args=[inventory.owner_id, inventory.id])

        InventoryModel.session.commit()
        return 'Success'

    except DatabaseError:
        return 'An error occurred while inserting inventory with tag number \'{tag}\''.format(tag=item['tag'])

@cel.task(name="inventory.edit_item")
def edit_item(item, file, original_tag, original_owner):
    inventory = InventoryModel.find_by_tag(original_tag)
    if file:
        file_name = secure_filename(
            valid_extension(get_filename(file))
        )
        inventory.update(
            file_name=file_name
        )
        file.save(
            os.path.join(current_app.config['INVENTORY_IMAGES_DIR'], file_name)
        )

    try:
        item['buydate'] = datetime.strptime(
            str(date_parser(item['buydate'], ignoretz=True, )),
            '%Y-%m-%d %H:%M:%S'
        )

        item['warranty'] = datetime.strptime(
            str(date_parser(item['warranty'], ignoretz=True, )),
            '%Y-%m-%d %H:%M:%S'
        )

        inventory.update(**item)

        cel_user.send_task("users.update_ownership", args=[inventory.owner_id, inventory.id, original_owner])

        InventoryModel.session.commit()
        return 'Success'

    except DatabaseError:
        return 'An error occurred while editing inventory with tag number \'{tag}\''.format(tag=item['tag'])

@cel.task(name="inventory.deactivate_item")
def deactivate_item(item):
    try:
        inventory = InventoryModel.find_by_tag(item['tag'])
        inventory.update(active=self.args['active'])
    except DatabaseError:
        return 'An error occurred while deactivating inventory with tag number \'{tag}\''.format(tag=item['tag'])
