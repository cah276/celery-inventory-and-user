import os


USER_MODULE_ROUTES = {
    "ActiveUsersList": {
        "url": "/api/users/active",
        "methods": {
            "GET": "read all"
        }
    },
    "UsersResource": {
        "url": "/api/users",
        "methods": {
            "POST": "add user",
            "PUT": "update user"
        }
    },
    "QueryActiveUsersList": {
        "url": "/api/users/query",
        "methods": {
            "GET": "query active"
        }
    },
    "SearchByNetIDUser": {
        "url": "/api/users/search",
        "methods": {
            "GET": "search by netid"
        }
    }
}
