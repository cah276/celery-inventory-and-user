# # inventory/serializers.py
from marshmallow_sqlalchemy import ModelSchema

from marshmallow import fields

from .users_model import User, OwnedItems


class OwnedItemsSchema(ModelSchema):
    class Meta:
        model = OwnedItems

class UserSchema(ModelSchema):
    owned_items = fields.Nested(OwnedItemsSchema, many=True)
    class Meta:
        model = User
